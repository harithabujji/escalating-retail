
#include <ESP8266WiFi.h>
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;
int redpin= 10;
int greenpin= 9;

const char* ssid = "vivo 1819"; 
const char* password = "hari12345";
char server[] = "mail.smtp2go.com"; 

WiFiClient espClient;

void setup()
{

  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
   //Serial Port begin
  Serial.begin (9600);
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //dual led
    pinMode(redpin,OUTPUT);
  pinMode(greenpin,OUTPUT);

  Serial.begin(115200);
  delay(10);
  Serial.println("");
  Serial.println("");
  Serial.print("Connecting To: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi Connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  byte ret = sendEmail();
}

void loop()
{
    // put your main code here, to run repeatedly:
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);

  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  inches = (duration/2) / 74;   // Divide by 74 or multiply by 0.0135
  if(cm>=70)
  {
    digitalWrite(greenpin,LOW);
   digitalWrite(redpin,HIGH);
  }
  else
  {
    digitalWrite(redpin,LOW);
    digitalWrite(greenpin,HIGH);
    
  }
  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();   
  delay(500);

}

byte sendEmail()
{
  if (espClient.connect(server, 2525) == 1)
  {
    Serial.println(F("connected"));
  }
  else
  {
    Serial.println(F("connection failed"));
    return 0;
  }
  if (!emailResp())
  return 0;

  Serial.println(F("Sending EHLO"));
  espClient.println("EHLO www.example.com");
  if (!emailResp())
  return 0;

  espClient.println("STARTTLS");
  if (!emailResp())
  return 0;
  Serial.println(F("Sending auth login"));
  espClient.println("AUTH LOGIN");
  if (!emailResp())
  return 0;

  Serial.println(F("Sending User"));

  espClient.println("aGFyaXRoYXJlZGR5MjAxOUBnbWFpbC5jb20="); 
  if (!emailResp())
  return 0;

  Serial.println(F("Sending Password"));

  espClient.println("VW03SW5ob1p2cXJY");
  if (!emailResp())
  return 0;

  Serial.println(F("Sending From"));

  espClient.println(F("MAIL From: harithareddy2019@gmail.com"));
  if (!emailResp())
  return 0;

  Serial.println(F("Sending To"));
  espClient.println(F("RCPT To: g.k.12357@gmail.com"));
  if (!emailResp())
  return 0;

  Serial.println(F("Sending DATA"));
  espClient.println(F("DATA"));
  if (!emailResp())
  return 0;
  Serial.println(F("Sending email"));

  espClient.println(F("To: g.k.12357@gmail.com"));

  espClient.println(F("From: harithareddy2019@gmail.com"));
  espClient.println(F("Subject: ESP8266 test e-mail\r\n"));
  espClient.println(F("This is is a test e-mail sent from ESP8266.\n"));
  espClient.println(F("Second line of the test e-mail."));
  espClient.println(F("Third line of the test e-mail."));

  espClient.println(F("."));
  if (!emailResp())
  return 0;

  Serial.println(F("Sending QUIT"));
  espClient.println(F("QUIT"));
  if (!emailResp())
  return 0;

  espClient.stop();
  Serial.println(F("disconnected"));
  return 1;
}

byte emailResp()
{
  byte responseCode;
  byte readByte;
  int loopCount = 0;

  while (!espClient.available())
  {
    delay(1);
    loopCount++;
    if (loopCount > 20000)
    {
      espClient.stop();
      Serial.println(F("\r\nTimeout"));
      return 0;
    }
  }
  responseCode = espClient.peek();
  while (espClient.available())
  {
    readByte = espClient.read();
    Serial.write(readByte);
  }

  if (responseCode >= '4')
  {
    return 0;
  }
  return 1;
}
